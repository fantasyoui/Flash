﻿package _fla {


import flash.display.*;
import flash.events.*;

dynamic public class M_3 extends MovieClip {
	public var dot_mc:SimpleButton;
	public var shafa1:SimpleButton;
	public var shafa2:SimpleButton;
	public var shafa3:SimpleButton;
	public var shafa4:SimpleButton;
	public var shafa5:SimpleButton;
	public var shafa6:SimpleButton;
	public var shafa7:SimpleButton;
	public var shafa8:SimpleButton;
	public var shafa9:SimpleButton;
	public var shafa10:SimpleButton;
	public var shafa11:SimpleButton;
	public var shafa12:SimpleButton;
	public var shafa13:SimpleButton;
	public var shafa14:SimpleButton;
	public var shafa15:SimpleButton;
	public var shafa16:SimpleButton;
	public var shafa17:SimpleButton;
	public var shafa18:SimpleButton;
	public var shafa19:SimpleButton;
	public var shafa20:SimpleButton;
	public var shafa21:SimpleButton;
	public var shafa22:SimpleButton;
	public var shafa23:SimpleButton;
	public var x_mc:SimpleButton;


	public function M_3() {
		addFrameScript(0, this.frame1, 8, this.frame9);
		return;
	}// end function

	public function Active(event:MouseEvent):void {
		gotoAndPlay(2);
		MovieClip(root).guizi.guiziin.gotoAndStop(1);
		return;
	}// end function

	function frame1() {
		stop();
		this.dot_mc.addEventListener(MouseEvent.CLICK, this.Active);
		return;
	}// end function

	public function shafa1go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(1);
		return;
	}// end function

	public function shafa2go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(2);
		return;
	}// end function

	public function shafa3go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(3);
		return;
	}// end function

	public function shafa4go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(4);
		return;
	}// end function

	public function shafa5go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(5);
		return;
	}// end function

	public function shafa6go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(6);
		return;
	}// end function

	public function shafa7go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(7);
		return;
	}// end function

	public function shafa8go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(8);
		return;
	}// end function

	public function shafa9go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(9);
		return;
	}// end function

	public function shafa10go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(10);
		return;
	}// end function

	public function shafa11go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(11);
		return;
	}// end function

	public function shafa12go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(12);
		return;
	}// end function

	public function shafa13go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(13);
		return;
	}// end function

	public function shafa14go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(14);
		return;
	}// end function

	public function shafa15go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(15);
		return;
	}// end function

	public function shafa16go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(16);
		return;
	}// end function

	public function shafa17go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(17);
		return;
	}// end function

	public function shafa18go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(18);
		return;
	}// end function

	public function shafa19go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(19);
		return;
	}// end function

	public function shafa20go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(20);
		return;
	}// end function

	public function shafa21go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(21);
		return;
	}// end function

	public function shafa22go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(22);
		return;
	}// end function

	public function shafa23go(event:MouseEvent):void {
		MovieClip(root).shafa.gotoAndStop(23);
		return;
	}// end function

	function frame9() {
		stop();
		this.x_mc.addEventListener(MouseEvent.CLICK, this.goback);
		this.shafa1.addEventListener(MouseEvent.CLICK, this.shafa1go);
		this.shafa2.addEventListener(MouseEvent.CLICK, this.shafa2go);
		this.shafa3.addEventListener(MouseEvent.CLICK, this.shafa3go);
		this.shafa4.addEventListener(MouseEvent.CLICK, this.shafa4go);
		this.shafa5.addEventListener(MouseEvent.CLICK, this.shafa5go);
		this.shafa6.addEventListener(MouseEvent.CLICK, this.shafa6go);
		this.shafa7.addEventListener(MouseEvent.CLICK, this.shafa7go);
		this.shafa8.addEventListener(MouseEvent.CLICK, this.shafa8go);
		this.shafa9.addEventListener(MouseEvent.CLICK, this.shafa9go);
		this.shafa10.addEventListener(MouseEvent.CLICK, this.shafa10go);
		this.shafa11.addEventListener(MouseEvent.CLICK, this.shafa11go);
		this.shafa12.addEventListener(MouseEvent.CLICK, this.shafa12go);
		this.shafa13.addEventListener(MouseEvent.CLICK, this.shafa13go);
		this.shafa14.addEventListener(MouseEvent.CLICK, this.shafa14go);
		this.shafa15.addEventListener(MouseEvent.CLICK, this.shafa15go);
		this.shafa16.addEventListener(MouseEvent.CLICK, this.shafa16go);
		this.shafa17.addEventListener(MouseEvent.CLICK, this.shafa17go);
		this.shafa18.addEventListener(MouseEvent.CLICK, this.shafa18go);
		this.shafa19.addEventListener(MouseEvent.CLICK, this.shafa19go);
		this.shafa20.addEventListener(MouseEvent.CLICK, this.shafa20go);
		this.shafa21.addEventListener(MouseEvent.CLICK, this.shafa21go);
		this.shafa22.addEventListener(MouseEvent.CLICK, this.shafa22go);
		this.shafa23.addEventListener(MouseEvent.CLICK, this.shafa23go);
		return;
	}// end function

	public function goback(event:MouseEvent):void {
		play();
		return;
	}// end function

}
}
