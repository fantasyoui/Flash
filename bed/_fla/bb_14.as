﻿package _fla {


import flash.display.*;
import flash.events.*;

dynamic public class bb_14 extends MovieClip {
	public var b1:SimpleButton;
	public var b2:SimpleButton;
	public var b3:SimpleButton;
	public var b4:SimpleButton;
	public var b5:SimpleButton;
	public var b6:SimpleButton;
	public var b7:SimpleButton;
	public var b8:SimpleButton;
	public var b9:SimpleButton;
	public var b10:SimpleButton;
	public var b11:SimpleButton;
	public var b12:SimpleButton;
	public var b13:SimpleButton;
	public var b14:SimpleButton;
	public var b15:SimpleButton;
	public var b16:SimpleButton;
	public var b17:SimpleButton;
	public var b18:SimpleButton;
	public var b19:SimpleButton;
	public var b20:SimpleButton;
	public var b21:SimpleButton;
	public var b22:SimpleButton;
	public var b23:SimpleButton;
	public var b24:SimpleButton;
	public var b25:SimpleButton;
	public var b26:SimpleButton;
	public var b27:SimpleButton;
	public var b28:SimpleButton;
	public var b29:SimpleButton;
	public var b30:SimpleButton;
	public var b31:SimpleButton;
	public var dot_mc:SimpleButton;
	public var shafa1:SimpleButton;
	public var shafa2:SimpleButton;
	public var x_mc:SimpleButton;

	public function bb_14() {
		addFrameScript(0, this.frame1, 8, this.frame9);
		return;
	}// end function

	public function Active(event:MouseEvent):void {
		gotoAndPlay(2);
		MovieClip(root).shafa.shafain.gotoAndStop(1);
		return;
	}// end function

	public function shafa1go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(1);
		return;
	}// end function

	public function shafa2go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(2);
		return;
	}// end function

	public function shafa3go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(3);
		return;
	}// end function

	public function shafa4go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(4);
		return;
	}// end function

	public function shafa5go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(5);
		return;
	}// end function

	public function shafa6go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(6);
		return;
	}// end function

	public function shafa7go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(7);
		return;
	}// end function

	public function shafa8go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(8);
		return;
	}// end function

	public function shafa9go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(9);
		return;
	}// end function

	public function shafa10go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(10);
		return;
	}// end function

	public function shafa11go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(11);
		return;
	}// end function

	public function shafa12go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(12);
		return;
	}// end function

	public function shafa13go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(13);
		return;
	}// end function

	public function shafa14go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(14);
		return;
	}// end function

	public function shafa15go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(15);
		return;
	}// end function

	public function shafa16go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(16);
		return;
	}// end function

	public function shafa17go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(17);
		return;
	}// end function

	public function shafa18go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(18);
		return;
	}// end function

	public function shafa19go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(19);
		return;
	}// end function

	public function shafa20go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(20);
		return;
	}// end function

	public function shafa21go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(21);
		return;
	}// end function

	public function shafa22go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(22);
		return;
	}// end function

	public function shafa23go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(23);
		return;
	}// end function

	public function shafa24go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(24);
		return;
	}// end function

	public function shafa25go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(25);
		return;
	}// end function

	public function shafa26go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(26);
		return;
	}// end function

	public function shafa27go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(27);
		return;
	}// end function

	public function shafa28go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(28);
		return;
	}// end function

	public function shafa29go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(29);
		return;
	}// end function

	public function shafa30go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(30);
		return;
	}// end function

	public function shafa31go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(31);
		return;
	}// end function

	public function goback(event:MouseEvent):void {
		play();
		return;
	}// end function

	function frame1() {
		stop();
		this.dot_mc.addEventListener(MouseEvent.CLICK, this.Active);
		return;
	}// end function

	function frame9() {
		stop();
		this.x_mc.addEventListener(MouseEvent.CLICK, this.goback);
		this.b1.addEventListener(MouseEvent.CLICK, this.shafa1go);
		this.b2.addEventListener(MouseEvent.CLICK, this.shafa2go);
		this.b3.addEventListener(MouseEvent.CLICK, this.shafa3go);
		this.b4.addEventListener(MouseEvent.CLICK, this.shafa4go);
		this.b5.addEventListener(MouseEvent.CLICK, this.shafa5go);
		this.b6.addEventListener(MouseEvent.CLICK, this.shafa6go);
		this.b7.addEventListener(MouseEvent.CLICK, this.shafa7go);
		this.b8.addEventListener(MouseEvent.CLICK, this.shafa8go);
		this.b9.addEventListener(MouseEvent.CLICK, this.shafa9go);
		this.b10.addEventListener(MouseEvent.CLICK, this.shafa10go);
		this.b11.addEventListener(MouseEvent.CLICK, this.shafa11go);
		this.b12.addEventListener(MouseEvent.CLICK, this.shafa12go);
		this.b13.addEventListener(MouseEvent.CLICK, this.shafa13go);
		this.b14.addEventListener(MouseEvent.CLICK, this.shafa14go);
		this.b15.addEventListener(MouseEvent.CLICK, this.shafa15go);
		this.b16.addEventListener(MouseEvent.CLICK, this.shafa16go);
		this.b17.addEventListener(MouseEvent.CLICK, this.shafa17go);
		this.b18.addEventListener(MouseEvent.CLICK, this.shafa18go);
		this.b19.addEventListener(MouseEvent.CLICK, this.shafa19go);
		this.b20.addEventListener(MouseEvent.CLICK, this.shafa20go);
		this.b21.addEventListener(MouseEvent.CLICK, this.shafa21go);
		this.b22.addEventListener(MouseEvent.CLICK, this.shafa22go);
		this.b23.addEventListener(MouseEvent.CLICK, this.shafa23go);
		this.b24.addEventListener(MouseEvent.CLICK, this.shafa24go);
		this.b25.addEventListener(MouseEvent.CLICK, this.shafa25go);
		this.b26.addEventListener(MouseEvent.CLICK, this.shafa26go);
		this.b27.addEventListener(MouseEvent.CLICK, this.shafa27go);
		this.b28.addEventListener(MouseEvent.CLICK, this.shafa28go);
		this.b29.addEventListener(MouseEvent.CLICK, this.shafa29go);
		this.b30.addEventListener(MouseEvent.CLICK, this.shafa30go);
		this.b31.addEventListener(MouseEvent.CLICK, this.shafa31go);
		return;
	}// end function

}
}
