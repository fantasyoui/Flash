﻿package room418_fla
{
    import flash.display.*;
    import flash.events.*;

    dynamic public class M_2 extends MovieClip
    {
        public var dot_mc:SimpleButton;
        public var shafa3:SimpleButton;
        public var shafa4:SimpleButton;
        public var shafa6:SimpleButton;
        public var shafa1:SimpleButton;
        public var shafa5:SimpleButton;
        public var shafa2:SimpleButton;
        public var x_mc:SimpleButton;

        public function M_2()
        {
            addFrameScript(0, this.frame1, 8, this.frame9);
            return;
        }// end function

        public function shafa1go(event:MouseEvent) : void
        {
            MovieClip(root).shafa.gotoAndStop(1);
            return;
        }// end function

        public function shafa5go(event:MouseEvent) : void
        {
            MovieClip(root).shafa.gotoAndStop(5);
            return;
        }// end function

        public function Active(event:MouseEvent) : void
        {
            gotoAndPlay(2);
            return;
        }// end function

        function frame1()
        {
            stop();
            this.dot_mc.addEventListener(MouseEvent.CLICK, this.Active);
            return;
        }// end function

        public function shafa4go(event:MouseEvent) : void
        {
            MovieClip(root).shafa.gotoAndStop(4);
            return;
        }// end function

        function frame9()
        {
            stop();
            this.x_mc.addEventListener(MouseEvent.CLICK, this.goback);
            this.shafa1.addEventListener(MouseEvent.CLICK, this.shafa1go);
            this.shafa2.addEventListener(MouseEvent.CLICK, this.shafa2go);
            this.shafa3.addEventListener(MouseEvent.CLICK, this.shafa3go);
            this.shafa4.addEventListener(MouseEvent.CLICK, this.shafa4go);
            this.shafa5.addEventListener(MouseEvent.CLICK, this.shafa5go);
            this.shafa6.addEventListener(MouseEvent.CLICK, this.shafa6go);
            return;
        }// end function

        public function shafa3go(event:MouseEvent) : void
        {
            MovieClip(root).shafa.gotoAndStop(3);
            return;
        }// end function

        public function goback(event:MouseEvent) : void
        {
            play();
            return;
        }// end function

        public function shafa2go(event:MouseEvent) : void
        {
            MovieClip(root).shafa.gotoAndStop(2);
            return;
        }// end function

        public function shafa6go(event:MouseEvent) : void
        {
            MovieClip(root).shafa.gotoAndStop(6);
            return;
        }// end function

    }
}
