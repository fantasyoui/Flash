﻿package cook4_fla {


import flash.display.*;
import flash.events.*;

dynamic public class bb_12 extends MovieClip {
	public var b1:SimpleButton;
	public var b2:SimpleButton;
	public var b3:SimpleButton;
	public var b4:SimpleButton;
	public var b5:SimpleButton;
	public var b6:SimpleButton;
	public var b7:SimpleButton;
	public var b8:SimpleButton;
	public var b9:SimpleButton;
	public var b10:SimpleButton;
	public var b11:SimpleButton;
	public var b12:SimpleButton;
	public var b13:SimpleButton;
	public var dot_mc:SimpleButton;
	public var shafa1:SimpleButton;
	public var shafa2:SimpleButton;
	public var x_mc:SimpleButton;

	public function bb_12() {
		addFrameScript(0, this.frame1, 8, this.frame9);
		return;
	}// end function

	public function Active(event:MouseEvent):void {
		gotoAndPlay(2);
		MovieClip(root).shafa.shafain.gotoAndStop(1);
		return;
	}// end function

	function frame1() {
		stop();
		this.dot_mc.addEventListener(MouseEvent.CLICK, this.Active);
		return;
	}// end function

	function frame9() {
		stop();
		this.x_mc.addEventListener(MouseEvent.CLICK, this.goback);
		this.b1.addEventListener(MouseEvent.CLICK, this.shafa1go);
		this.b2.addEventListener(MouseEvent.CLICK, this.shafa2go);
		this.b3.addEventListener(MouseEvent.CLICK, this.shafa3go);
		this.b4.addEventListener(MouseEvent.CLICK, this.shafa4go);
		this.b5.addEventListener(MouseEvent.CLICK, this.shafa5go);
		this.b6.addEventListener(MouseEvent.CLICK, this.shafa6go);
		this.b7.addEventListener(MouseEvent.CLICK, this.shafa7go);
		this.b8.addEventListener(MouseEvent.CLICK, this.shafa8go);
		this.b9.addEventListener(MouseEvent.CLICK, this.shafa9go);
		this.b10.addEventListener(MouseEvent.CLICK, this.shafa10go);
		this.b11.addEventListener(MouseEvent.CLICK, this.shafa11go);
		this.b12.addEventListener(MouseEvent.CLICK, this.shafa12go);
		this.b13.addEventListener(MouseEvent.CLICK, this.shafa13go);
		return;
	}// end function

	public function shafa1go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(1);
		return;
	}// end function

	public function shafa2go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(2);
		return;
	}// end function

	public function shafa3go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(3);
		return;
	}// end function

	public function shafa4go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(4);
		return;
	}// end function

	public function shafa5go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(5);
		return;
	}// end function

	public function shafa6go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(6);
		return;
	}// end function

	public function shafa7go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(7);
		return;
	}// end function

	public function shafa8go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(8);
		return;
	}// end function

	public function shafa9go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(9);
		return;
	}// end function

	public function shafa10go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(10);
		return;
	}// end function

	public function shafa11go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(11);
		return;
	}// end function

	public function shafa12go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(12);
		return;
	}// end function

	public function shafa13go(event:MouseEvent):void {
		MovieClip(root).guizi.gotoAndStop(13);
		return;
	}// end function

	public function goback(event:MouseEvent):void {
		play();
		return;
	}// end function

}
}
