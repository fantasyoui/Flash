﻿class org.kingda.StringTool
{
    function StringTool()
    {
    } // End of the function
    static function ltrim(str)
    {
        var _loc3 = str.length;
        for (var _loc1 = 0; _loc1 < _loc3; ++_loc1)
        {
            if (str.charCodeAt(_loc1) > 32)
            {
                return (str.substring(_loc1));
            } // end if
        } // end of for
        return ("");
    } // End of the function
    static function rtrim(str)
    {
        var _loc3 = str.length;
        for (var _loc1 = _loc3; _loc1 > 0; --_loc1)
        {
            if (str.charCodeAt(_loc1) > 32)
            {
                return (str.substring(0, _loc1 + 1));
            } // end if
        } // end of for
        return ("");
    } // End of the function
    static function trim(str)
    {
        return (org.kingda.StringTool.rtrim(org.kingda.StringTool.ltrim(str)));
    } // End of the function
    static function replace(str, replace, replaceWith)
    {
        var _loc6 = new String();
        var _loc5 = false;
        for (var _loc2 = 0; _loc2 < str.length; ++_loc2)
        {
            if (str.charAt(_loc2) == replace.charAt(0))
            {
                _loc5 = true;
                for (var _loc1 = 0; _loc1 < replace.length; ++_loc1)
                {
                    if (str.charAt(_loc2 + _loc1) != replace.charAt(_loc1))
                    {
                        _loc5 = false;
                        break;
                    } // end if
                } // end of for
                if (_loc5)
                {
                    _loc6 = _loc6 + replaceWith;
                    _loc2 = _loc2 + (replace.length - 1);
                    continue;
                } // end if
            } // end if
            _loc6 = _loc6 + str.charAt(_loc2);
        } // end of for
        return (_loc6);
    } // End of the function
    static function remove(str, remove)
    {
        return (org.kingda.StringTool.replace(str, remove, ""));
    } // End of the function
    static function beginsWith(str, s)
    {
        return (s == str.substring(0, s.length));
    } // End of the function
    static function endsWith(str, s)
    {
        return (s == str.substring(str.length - s.length));
    } // End of the function
    static function trimOnlySpace(str)
    {
        var _loc1 = str.split(" ");
        return (_loc1.join(""));
    } // End of the function
    static function trimAllSpaces(str)
    {
        var _loc1 = org.kingda.StringTool.trimOnlySpace(str);
        _loc1 = org.kingda.StringTool.remove(_loc1, "\n");
        _loc1 = org.kingda.StringTool.remove(_loc1, "\t");
        _loc1 = org.kingda.StringTool.remove(_loc1, "\r");
        _loc1 = org.kingda.StringTool.remove(_loc1, "\b");
        return (_loc1);
    } // End of the function
    static function trimPunc(str)
    {
        var _loc3 = ["!", ",", ".", ";", ":", "\"", "(", ")", "?"];
        var _loc2 = str;
        var _loc4 = _loc3.length;
        for (var _loc1 = 0; _loc1 < _loc4; ++_loc1)
        {
            _loc2 = org.kingda.StringTool.replace(_loc2, _loc3[_loc1], "");
        } // end of for
        return (_loc2);
    } // End of the function
    static function SetVisible(mc, flag)
    {
        mc._visible = flag;
    } // End of the function
    static var className = "StringTool";
    static var classVer = "1.0";
    static var authorName = "Kingda Sun";
    static var updateURL = "http://www.kingda.org/";
} // End of Class
