﻿class com.ruochi.RTools
{
    function RTools()
    {
    } // End of the function
    static function SetVisible(mc, flag)
    {
        mc._visible = flag;
    } // End of the function
    static function SetQuality(mc, flag)
    {
        mc._quality = flag;
    } // End of the function
    static function stringToFunction(str)
    {
        str = str.split("(");
        str[0] = str[0].split(".");
        str[1] = str[1].split(",");
        str[1][str[1].length - 1] = str[1][str[1].length - 1].split(")")[0];
        var i = 0;
        while (i < str[1].length)
        {
            if (str[1][i].indexOf(":Number") > 0)
            {
                str[1][i] = Number(str[1][i].split(":")[0]);
            }
            else if (str[1][i].indexOf(":String") > 0)
            {
                str[1][i] = String(str[1][i].split(":")[0]);
            }
            else
            {
                str[1][i] = eval(str[1][i]);
            } // end else if
            ++i;
        } // end while
        eval(str[0][str[0].length - 1]).apply(str[0].slice(0, str[0].length - 1).join("."), str[1]);
    } // End of the function
    static function limitSlideY(mc, x1, x2, y1, y2, top, bottom, speed)
    {
        var step;
        var center;
        center = (y1 + y2) / 2;
        mc.onMouseMove = function ()
        {
            if (mc._parent._xmouse > x1 && mc._parent._xmouse < x2 && mc._parent._ymouse > y1 && mc._parent._ymouse < y2)
            {
                mc.onEnterFrame = function ()
                {
                    step = mc._y + (center - mc._parent._ymouse) / speed;
                    if (step >= bottom)
                    {
                        mc._y = bottom;
                    }
                    else if (step <= top)
                    {
                        mc._y = top;
                    }
                    else
                    {
                        mc._y = step;
                    } // end else if
                };
            }
            else
            {
                delete mc.onEnterFrame;
            } // end else if
        };
    } // End of the function
    static function setUn(A, B)
    {
        if (A == undefined || A == "")
        {
            return (B);
        }
        else
        {
            return (A);
        } // end else if
    } // End of the function
    static function load(url, _mc)
    {
        var _loc2 = new MovieClipLoader();
        var _loc1 = new Object();
        var mc;
        var w;
        var h;
        w = _mc._width;
        h = _mc._height;
        _loc1.onLoadStart = function (target_mc)
        {
            mc = target_mc._parent.attachMovie("RloadingPresenter", "RLP", 10);
            mc._x = w / 2;
            mc._y = h / 2;
            mc.start();
        };
        _loc1.onProgress = function (target_mc, bytesLoaded, bytesTotal)
        {
            target_mc._parent.RLP.t.text = Math.floor(100 - bytesLoaded / bytesTotal * 100);
        };
        _loc1.onLoadInit = function (target)
        {
            target._parent.RLP.end();
            target._parent.alphaTo(100);
            trace (target);
        };
        _loc2.addListener(_loc1);
        _loc2.loadClip(url, _mc);
    } // End of the function
} // End of Class
